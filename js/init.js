/*
function loadimage(img){
  var src;
  src = $(img).attr('data-on');
  //console.log($.now());
  //console.log(1512086400000);
  if( $.now() > 1512172800000) {
    $(img).attr('src', src);
  }
  loadimage = function(){}
}
*/


(function($){
  $(function(){


    $('<span class="fill-box"></span>').appendTo('.box-bg');
    if( $( window ).width() <= 600 ){
      $('.remove-class-show').removeClass('anim');
      $('.remove-class-show').removeClass('show');
    }

      /*
    var $product_bg = $('.product');
    $product_bg.height($product_bg.parent().height());
    $( window ).resize(function() {
      $product_bg.height($product_bg.parent().height());
    });*/

    $('.card>.card-image>img').addClass('hvr-float');
    

    $('.check-expiry').each(function(i, obj) {
      var src, e_time;
      src = $(this).attr('data-on');
      e_time = $(this).attr('data-expiry');
      if( $.now() > e_time) {
        $(this).attr('src', src);
      }
    });    

    var timer;
    $('.scrollspy').scrollSpy({
      getActiveElement:function(id){
        $('.tabs .active').removeClass("active");
        var ele = $('.tabs a[href="#' + id + '"]').position();
        var width = $('.tabs a[href="#' + id + '"]').innerWidth();
        var tabsWidth = $('.tabs').innerWidth();

        $('.indicator').css("right", tabsWidth - width -ele.left + "px");
        // $('.indicator').css("margin-left", 0);
        // $('.indicator').css("margin-right", 0);
        $('.indicator').css("left", ele.left+"px");
        return 'a[href="#' + id + '"]';
      }
    });
    
    $( ".view-all" ).on( "click", function() {
      $('#section1 .outer').addClass('show');
      $(this).fadeOut();
    });

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $(".nav-extended").css("top", $("#header").height()+'px');

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      $(".nav-extended").css("top", $("#header").height()+'px');
      if (y > 500) {
        $('.nav-section').addClass('active');
      } else {
        $('.nav-section').removeClass('active');
      }
    });

    $( ".trigger" ).on( "click", function() {
      $('.nav-section .tabs.tabs-transparent').toggleClass('active');
      if( $(".trigger>i").hasClass('fa-angle-up') ){
        $(".trigger>i").removeClass('fa-angle-up');
        $(".trigger>i").addClass('fa-angle-down');
      }else{
        $(".trigger>i").removeClass('fa-angle-down');
        $(".trigger>i").addClass('fa-angle-up');
      }
    });

    $( ".nav-section .tab>a" ).on( "click", function() {
      $('.trigger>span').text($(this).text());
      $(".trigger").trigger('click');
    });

    var options = [
      {selector: '#index-banner', offset: 200, callback: function() {
        setTimeout(
          function() 
          {
            $('#index-banner').find('img').addClass('animated fadeIn');
            $('#index-banner').find('h5').addClass('animated fadeInUp');
          }, 700);
      } },
      {selector: '#section1', offset: 200, callback: function() {
        setTimeout(
          function() 
          {
            $('#section1').find('.outer.show').addClass('animated fadeInUp');
          }, 100);
      } },
      {selector: '#bg-1', offset: 200, callback: function() {
        setTimeout(
          function() 
          {
            $('#bg-1').find('.card').addClass('animated fadeInUp');
          }, 100);
      } },
      {selector: '#section2', offset: 200, callback: function() {
        setTimeout(
          function() 
          {
            $('#section2').find('.card').addClass('animated fadeInUp');
          }, 100);
      } },
      {selector: '#section3', offset: 200, callback: function() {
        setTimeout(
          function() 
          {
            $('#section3').find('.card').addClass('animated fadeInUp');
          }, 100);
      } },
      {selector: '#section4', offset: 400, callback: function() {
        setTimeout(
          function() 
          {
            $('#section4').find('.card').addClass('animated fadeInUp');
          }, 100);
      } },
      {selector: '#section5', offset: 600, callback: function() {
        setTimeout(
          function() 
          {
            $('#section5').find('.card').addClass('animated fadeInUp');
          }, 100);
      } },
    ];
    Materialize.scrollFire(options);

    $(".back2top").on("click", function(){
      $("html, body").animate({ scrollTop: 0 }, 500);
    });

    $(".scroll-down-btn").on("click", function(){
        $('ul.tabs').tabs('select_tab', 'section1');
    });

    $('.slick').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.mobile-only-slick').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            dots: true,
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.section3-slick').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      dots: false
    });

    $('#section4 .slick-4').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $("a").on('click', function(){
      var title = $(this).attr("data-ga-attr");
      if (typeof title !== typeof undefined && title !== false) {
        ga('send', {
          hitType: 'event',
          eventCategory: 'Product',
          eventAction: 'click',
          eventLabel: title
        });
      }
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space



